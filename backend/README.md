## Instalar o express

- npm install express

## Instalar o Nodemon em desenvolvimento

- npm install nodemon -D

## Instalando o KnexJs - http://knexjs.org/ Orm para banco de dados relacional

- npm install knex
- Driver SQLite: npm install sqlite3
- Execultar o npx knex init para criar o arquivo de cofiguracao.

## Configurando as migrações

- npx knex migrate:make create_ongs (Criação das Ogs)
- Depois de configurar a migrate rodar o comando: npx knex migrate:latest

## Instalando o CORS

- npm install cors

## Envio de email

- npm install --save nodemailer
- - Criação de contas fake para envio de email
- - - https://ethereal.email/create
