const conn = require('../database/connections');
const crypto = require('crypto');
const nodemailer = require('nodemailer');

const transporter = nodemailer.createTransport({
  host: 'smtp.gmail.com',
  port: 587,
  auth: {
    user: 'luluka.magaiver@gmail.com',
    pass: 'Luc13n38'
  }
});

module.exports = {
  async criar(request, response) {
    const { nome, email, whatsapp, cidade, uf } = request.body;
    const id = crypto.randomBytes(4).toString('HEX');

    const mailOptions = {
      from: 'luluka.magaiver@gmail.com',
      to: email,
      subject: 'Be The Hero',
      text: `Segue seu ID ${id}`
    };

    await conn('ongs').insert({
      id,
      nome,
      email,
      whatsapp,
      cidade,
      uf
    });

    transporter.sendMail(mailOptions, function(error, info) {
      if (error) {
        console.error(error);
        return response.status(400).json({ error });
      } else {
        console.log(info);
        return response.status(200).json({ emailEmviado: info.accepted });
      }
    });
  },

  async listar(request, response) {
    const ongs = await conn('ongs').select('*');

    return response.json(ongs);
  }
};
