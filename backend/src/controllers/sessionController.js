const conn = require('../database/connections');

module.exports = {
  async criar(request, response) {
    const { id } = request.body;

    const ong = await conn('ongs')
      .where('id', id)
      .select('nome')
      .first();

    if (!ong) {
      return response.status(400).json({ error: 'Não encontrado a ONG com ID informado!' });
    }

    return response.json(ong);
  }
};
