const express = require('express');
const cors = require('cors');
const routes = require('./routes');

const app = express();
app.use(cors());
app.use(express.json());
app.use(routes);

/**
 * Rotas / Recursos
 */

/**
 * Tipos de parametros
 * Query params - request.query
 * Route params - request.params
 * Request Body - request.body (configurar o app.use(express.json()); apos a criacao do app)
 */

/**
 * Banco de dados: SQLite
 * Query Builder: KnexJs
 */

app.listen(3333);
