import React, { useState } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { FiArrowLeft } from 'react-icons/fi';
import Swal from 'sweetalert2';
import './styles.css';

import api from '../../services/api';

import logoImg from '../../assets/logo.svg';

export default function Register() {
  const [nome, setNome] = useState('');
  const [email, setEmail] = useState('');
  const [whatsapp, setWhatsapp] = useState('');
  const [cidade, setCidade] = useState('');
  const [uf, setUf] = useState('');

  const history = useHistory();

  const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    onOpen: toast => {
      toast.addEventListener('mouseenter', Swal.stopTimer);
      toast.addEventListener('mouseleave', Swal.resumeTimer);
    }
  });

  function handleIsNull(data) {
    if (data.nome === '') {
      Toast.fire({
        icon: 'warning',
        title: 'Informe o nome da ong'
      });
      return false;
    } else if (data.email === '') {
      Toast.fire({
        icon: 'warning',
        title: 'Informe um email valido'
      });
      return false;
    } else if (data.whatsapp === '') {
      Toast.fire({
        icon: 'warning',
        title: 'Informe um telefone'
      });
      return false;
    } else if (data.cidade === '') {
      Toast.fire({
        icon: 'warning',
        title: 'Informe uma Cidade'
      });
      return false;
    } else if (data.uf === '') {
      Toast.fire({
        icon: 'warning',
        title: 'Informe uma UF'
      });
      return false;
    }

    return true
  }

  function handleRegister(e) {
    e.preventDefault();

    const data = {
      nome,
      email,
      whatsapp,
      cidade,
      uf
    };

    try {
      if(handleIsNull(data)) {
        Swal.fire({
          title: 'Aguarde...',
          allowEscapeKey: false,
          allowOutsideClick: false,
          onOpen: async () => {
            Swal.showLoading();
            await api
              .post('ongs', data)
              .then(resposta => {
                if (resposta.status === 200) {
                  Swal.hideLoading();
                  history.push('/');
                  Toast.fire({
                    icon: 'success',
                    title: `Um email foi enviado para\n"${resposta.data.emailEmviado[0]}" com seu ID!`
                  });
                }

                if (resposta.status === 400) {
                  Swal.hideLoading();
                  Toast.fire({
                    icon: 'error',
                    title: `Erro no cadastro da Ong ${resposta.data.error[0]}`
                  });
                }
              })
              .catch(error => {
                Swal.hideLoading();
                Toast.fire({
                  icon: 'error',
                  title: `Erro de Rede\n ${error.message}`
                });
              });
          }
        });
      }
    } catch (error) {
      Swal.hideLoading();
      Toast.fire({
        icon: 'error',
        title: `Erro no cadastro da Ong ${error}`
      });
    }
  }

  return (
    <div className="register-container">
      <div className="content">
        <section>
          <img src={logoImg} alt="logo" />

          <h1>Cadastro</h1>
          <p>Faça seu cadastro, entre na plataforma e ajude pessoas na sua ONG</p>

          <Link className="back-link" to="/">
            <FiArrowLeft size={16} color="#E02041" />
            Voltar
          </Link>
        </section>
        <form onSubmit={handleRegister}>
          <input placeholder="Nome da Ong" value={nome} onChange={e => setNome(e.target.value)} />
          <input
            type="email"
            placeholder="E-mail"
            value={email}
            onChange={e => setEmail(e.target.value)}
          />
          <input
            placeholder="Whatsapp"
            value={whatsapp}
            onChange={e => setWhatsapp(e.target.value)}
          />

          <div className="input-group">
            <input placeholder="Cidade" value={cidade} onChange={e => setCidade(e.target.value)} />
            <input
              placeholder="UF"
              value={uf}
              onChange={e => setUf(e.target.value)}
              style={{ width: 80 }}
            />
          </div>

          <button className="button" type="submit">
            Cadastrar
          </button>
        </form>
      </div>
    </div>
  );
}
